//
//  DetailViewController.swift
//  TwitterTest
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Foundation

class DetailViewController: UIViewController {
    
    var homeStatuses: [HomeStatus]?
    
    var homeStatus: HomeStatus? {
        didSet {
            if let profileImageUrl = homeStatus?.profileImageUrl {
                
                if let name = homeStatus?.name, let screenName = homeStatus?.screenName, let text = homeStatus?.text, let date = homeStatus?.createdAt {
                    let attributedText = NSMutableAttributedString(string: date, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 12),
                                                                    NSForegroundColorAttributeName : UIColor.darkGray])
                    
                    attributedText.append(NSAttributedString(string: "\n@\(name)", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)]))
                    
                    attributedText.append(NSAttributedString(string: "\n@\(screenName)", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 16)]))
                    
                    attributedText.append(NSAttributedString(string: "\n\(text)", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]))
                    
                    statusTextView.attributedText = attributedText
                }
                
                let largeUrl = (profileImageUrl as NSString).replacingOccurrences(of: "_normal", with: "")
                
                let url = URL(string: largeUrl)
                URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        print(error ?? "")
                        return
                    }
                    
                    let image = UIImage(data: data!)
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.profileImageView.image = image
                    })
                    
                }).resume()
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        view.backgroundColor = .white
    }
    
    let statusTextView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        return textView
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    
    func setupViews() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        view.addSubview(statusTextView)
        view.addSubview(profileImageView)
        
        view.addConstraintsWithFormat("H:|-8-[v0]|", views: statusTextView)
        
        view.addConstraintsWithFormat("H:|-8-[v0(200)]|", views: profileImageView)
        
        view.addConstraintsWithFormat("V:|-72-[v0(200)]-8-[v1]|", views: profileImageView, statusTextView)
        
    }
}
