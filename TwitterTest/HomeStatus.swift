//
//  HomeStatus.swift
//  TwitterTest
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct HomeStatus {
    var text: String?
    var profileImageUrl: String?
    var name: String?
    var screenName: String?
    var createdAt: String?
    var Id: String?
}
