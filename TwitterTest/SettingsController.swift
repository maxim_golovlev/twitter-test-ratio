//
//  SettingsController.swift
//  TwitterTest
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit


class SettingsViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }

    
    static var initValue: Bool {
        if let avatarHeight = UserDefaults.standard.value(forKey: "avatarHeight") as? CGFloat {
            return avatarHeight > 0 ? true : false
        }
        return false
    }
    
    let exitBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("Exit", for: .normal)
        btn.contentHorizontalAlignment = .right
        btn.setTitleColor(.black, for: .normal)
        return btn
    }()
    
    let switcherDescription: UILabel = {
        let label = UILabel()
        label.text = "Show avatar images"
        label.textColor = .black
        return label
    }()
    
    let switcher: UISwitch = {
       let switcher = UISwitch()
        switcher.addTarget(self, action: #selector(avatarHeightChanged(sender:)), for: .valueChanged)
        switcher.setOn(initValue, animated: false)
        return switcher
    }()
    
    func setupViews() {
        
        view.backgroundColor = .white
        
        exitBtn.addTarget(self, action: #selector(exit), for: .touchUpInside)
        view.addSubview(exitBtn)
        view.addSubview(switcherDescription)
        view.addSubview(switcher)
        
        view.addConstraintsWithFormat("H:[v0(100)]-20-|", views: exitBtn)
        view.addConstraintsWithFormat("H:|-20-[v0]->=8-[v1]-20-|", views: switcher, switcherDescription)
        
        view.addConstraintsWithFormat("V:|-20-[v0(50)]-100-[v1]", views: exitBtn, switcherDescription)
        switcher.centerYAnchor.constraint(equalTo: switcherDescription.centerYAnchor).isActive = true
    }
    
    func exit() {
        
        if let parentNav = self.presentingViewController as? UINavigationController {
            
            if let parent = parentNav.viewControllers.last as? FeedViewController {
                parent.collectionView?.reloadData()
            }
        }
        
        dismiss(animated: true, completion: nil )
    }
    
    func avatarHeightChanged(sender: UISwitch!) {
        UserDefaults.standard.set( sender.isOn ? 48 : 0, forKey: "avatarHeight")
        UserDefaults.standard.synchronize()
    }
}
