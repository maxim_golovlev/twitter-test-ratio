//
//  StatusCell.swift
//  TwitterTest
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class StatusCell: UICollectionViewCell {
    
    var homeStatus: HomeStatus? {
        didSet {
            if let profileImageUrl = homeStatus?.profileImageUrl {
                
                if let name = homeStatus?.name, let screenName = homeStatus?.screenName, let text = homeStatus?.text {
                    let attributedText = NSMutableAttributedString(string: name, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
                    
                    attributedText.append(NSAttributedString(string: "\n@\(screenName)", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]))
                    
                    attributedText.append(NSAttributedString(string: "\n\(text)", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]))
                    
                    statusTextView.attributedText = attributedText
                }
                
                let url = URL(string: profileImageUrl)
                URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        print(error ?? "")
                        return
                    }
                    
                    let image = UIImage(data: data!)
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.profileImageView.image = image
                    })
                    
                }).resume()
            }
        }
    }
    
    var avatarHeight: CGFloat {
        
        if let avatarHeight = UserDefaults.standard.value(forKey: "avatarHeight") as? CGFloat {
            return avatarHeight
        }
        
        return 48
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let statusTextView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isUserInteractionEnabled = false
        return textView
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        return view
    }()
    
    func setupViews() {
        
        for view in contentView.subviews {
            view.removeFromSuperview()
        }
        
        contentView.addSubview(statusTextView)
        contentView.addSubview(dividerView)
        contentView.addSubview(profileImageView)
        
        // constraints for statusTextView
        contentView.addConstraintsWithFormat("H:|-8-[v0(\(avatarHeight))]-8-[v1]|", views: profileImageView, statusTextView)
        
        contentView.addConstraintsWithFormat("V:|[v0]|", views: statusTextView)
        
        contentView.addConstraintsWithFormat("V:|-8-[v0(\(avatarHeight))]", views: profileImageView)
        
        // constraints for dividerView
        contentView.addConstraintsWithFormat("H:|-8-[v0]|", views: dividerView)
        contentView.addConstraintsWithFormat("V:[v0(1)]|", views: dividerView)
    }
}
