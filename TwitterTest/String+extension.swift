//
//  String+extension.swift
//  TwitterTest
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

extension String {
    func dateString() -> String? {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
        dateFormatter1.locale = Locale.init(identifier: "en_GB")
        let dateObj = dateFormatter1.date(from: self)
        
        if dateObj != nil {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "MMMM dd yyyy"
            return dateFormatter2.string(from: dateObj!)
        }
        
        return nil
    }
}
