//
//  ViewController.swift
//  TwitterTest
//
//  Created by Admin on 25.01.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import STTwitter

class FeedViewController: UICollectionViewController {
    
    var footerHeight = CGFloat(0)
    let cellId = "cellId"
    let footerId = "footerId"
    var homeStatuses: [HomeStatus]?
    var twitter: STTwitterAPI?
    var refresher:UIRefreshControl!
    var loadingProcess = false
    var lastId: String?
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.homeStatuses = [HomeStatus]()
        
        twitter = STTwitterAPI(oAuthConsumerKey: "vj86BPP0dSbpiU8V9vC5mEM9p",
                               consumerSecret: "WgnmMANgwwyDyiICxzOcX23zvP2mr1qe0crfreBcdqj4nlrr5g",
                               oauthToken: "273858888-3u1puhslsrNwqb4v1UlrN1QxhhzOC16ocjSu4Vom",
                               oauthTokenSecret: "U3zLI8eZQiTK7xJvMZKTLijedG4UFmvrC4mIUVDdYWrXI")
        
        setupViews()
        
        refreshWall()
    }
    
    func setupViews() {
        
        navigationItem.title = "Twitter Home"
        
        collectionView?.backgroundColor = UIColor.white
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(StatusCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(FooterCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerId)

        refresher = UIRefreshControl()
        refresher.tintColor = .blue
        refresher.addTarget(self, action: #selector(refreshWall), for: .valueChanged)
        collectionView!.addSubview(refresher)
        
        let rightButton  = UIButton(type: .custom)
        rightButton.setImage(#imageLiteral(resourceName: "settings"), for: .normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
        rightButton.addTarget(self, action: #selector(openSettings), for: .touchUpInside)
        let rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(checkForUpdates), userInfo: nil, repeats: false)
    }
    
    deinit {
        timer?.invalidate()
    }
    
    func checkForUpdates() {
        
        print("lastId" + (lastId ?? "") )
        
        fetchTimeline(sinceId: lastId, count: 10, completion: { [unowned self] homeStatuses in
            
            self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.checkForUpdates), userInfo: nil, repeats: false)
            
            guard let homeStatuses = homeStatuses  else { return }

            if homeStatuses.count > 0 {
                let leftButton  = UIButton(type: .custom)
                leftButton.setImage(#imageLiteral(resourceName: "refresh"), for: .normal)
                leftButton.frame = CGRect(x: 0, y: 0, width: 28, height: 28)
                leftButton.addTarget(self, action: #selector(self.reloadData), for: .touchUpInside)
                let leftBarButtonItem = UIBarButtonItem(customView: leftButton)
                self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
            } else {
                self.navigationItem.leftBarButtonItem = nil
            }
        })
    }
    
    func reloadData() {
        collectionView?.reloadData()
    }
    
    func openSettings() {
        
        let vc = SettingsViewController()
        present(vc, animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = homeStatuses?.count {
            lastId = homeStatuses!.first?.Id
            return count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let statusCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! StatusCell
        
        if refresher.isRefreshing { return statusCell }
        
        if let homeStatus = self.homeStatuses?[indexPath.item] {
            statusCell.homeStatus = homeStatus
        }
        
        return statusCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath)
        return footer
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let homeStatus = self.homeStatuses?[indexPath.item] {
            
            let vc = DetailViewController()
            vc.homeStatus = homeStatus
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension FeedViewController {
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height + footerHeight
        
        if (bottomEdge >= scrollView.contentSize.height - 10) {
            // we are at the bottom
            
            if !loadingProcess {
                
                loadingProcess = true
                footerHeight = 20
                collectionView?.collectionViewLayout.invalidateLayout()
                
                collectionView?.performBatchUpdates(nil, completion: { [unowned self] _ in
                    let bottomOffset = CGPoint(x: 0, y: self.collectionView!.contentSize.height - self.collectionView!.bounds.size.height)
                    self.collectionView?.setContentOffset(bottomOffset, animated: true)
                    
                    self.fetchTimeline(count: (self.homeStatuses?.count ?? 0) + 10) { [unowned self] _ in
                        self.loadingProcess = false
                        self.footerHeight = 0
                        self.collectionView?.collectionViewLayout.invalidateLayout()
                     }
                })
            }
        }
    }
}

extension FeedViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: footerHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let homeStatus = self.homeStatuses?[indexPath.item] {
            if let name = homeStatus.name, let screenName = homeStatus.screenName, let text = homeStatus.text {
                let attributedText = NSMutableAttributedString(string: name, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
                
                attributedText.append(NSAttributedString(string: "\n@\(screenName)", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]))
                
                attributedText.append(NSAttributedString(string: "\n\(text)", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]))
                
                let size = attributedText.boundingRect(with: CGSize(width: view.frame.width - 80, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), context: nil).size
                
                return CGSize(width: view.frame.width, height: size.height + 20)
            }
        }
        
        return CGSize(width: view.frame.width, height: 80)
    }
}


extension FeedViewController {
    
    func refreshWall() {
        fetchTimeline(){ homeStatuses in
            self.homeStatuses = [HomeStatus]()
            self.homeStatuses = homeStatuses
            self.collectionView?.reloadData()
            self.refresher.endRefreshing()
        }
    }
    
    func fetchTimeline(sinceId: String? = nil, count: Int = 10, completion:@escaping ([HomeStatus]?)->()) {
        twitter?.verifyCredentials(userSuccessBlock: { [unowned self] (username, userId) -> Void in
            
            var homeStatuses = [HomeStatus]()
            
            _ = self.twitter?.getHomeTimeline(sinceID: sinceId, count: UInt(count), successBlock: { (statuses) -> Void in
                
                for status in statuses! {
            
                    if let status = status as? [String: AnyObject] {
                        
                        let text = status["text"] as? String
                        let createdAt = (status["created_at"] as? String)?.dateString()
                        let Id = status["id_str"] as? String
                        
                        if let user = status["user"] as? NSDictionary {
                            let profileImage = user["profile_image_url_https"] as? String
                            let screenName = user["screen_name"] as? String
                            let name = user["name"] as? String
                            
                            homeStatuses.append(HomeStatus(text: text, profileImageUrl: profileImage, name: name, screenName: screenName, createdAt: createdAt, Id: Id))
                        }
                    }
                }
                
                completion(homeStatuses)
                
            }, errorBlock: { (error) -> Void in
                print(error ?? "")
                completion(nil)
            })
            
        }) { (error) -> Void in
            print(error ?? "")
            completion(nil)
        }
    }
}
